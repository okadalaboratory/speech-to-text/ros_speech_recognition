#!/usr/bin/env python
# -*- coding: utf-8 -*-

## https://qiita.com/MENDY/items/e842e3c3ad7fe98c12a0
import os
import json

import rospy
from std_msgs.msg import String
from speech_recognition_msgs.msg import SpeechRecognitionCandidates
from std_srvs.srv import Empty

import openai
openai.api_key = "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"
#openai.api_key = os.environ["OPENAI_API_KEY"]
openai.organization = "org-K7OnmGQzK1WS6HE2fwhOGMoO"
FILENAME = "chat_history.txt"
MODEL_ENGINE = "gpt-3.5-turbo"



class myTTS():
    def __init__(self):
        # Publisherを作成
        self.publisher = rospy.Publisher('voicevox/request', String, queue_size=10)
        # Wait for connection
        while self.publisher.get_num_connections() == 0:
            rospy.sleep(0.1)

    def send_msg(self,say):
        # messageを送信
        self.publisher.publish(say)

class mySTT():
    def __init__(self, pub):
        if os.path.exists(FILENAME):
            with open(FILENAME, "r") as f:
                self.messages.extend([json.loads(j) for j in f.read().splitlines()])


        # Subscriberを作成
        self.subscriber =  rospy.Subscriber('/speech_to_text', SpeechRecognitionCandidates, self.callback)
        # Wait for connection
        while self.subscriber.get_num_connections() == 0:
            rospy.sleep(0.1)        

        self.pub=pub

        rospy.wait_for_service('speech_recognition/stop')
        self._stop = rospy.ServiceProxy('speech_recognition/stop', Empty)

        rospy.wait_for_service('speech_recognition/start')
        self._start = rospy.ServiceProxy('speech_recognition/start', Empty)



    def stop(self):
        # 音声認識をOFFにする
        self._stop()

    def start(self):
        # 音声認識をONにする
        self._start()

    def callback(self, msg):
        if len(msg.transcript[0]) == 0:
            return

        if len(msg.transcript) > 0: # 音声認識結果がある場合だけ処理。
            for k in range(0, len(msg.transcript)):
                rospy.loginfo(msg.transcript[k]) #認識した文字列
                rospy.loginfo(msg.confidence[k])

                # 音声認識をOFFにする
                self.stop()

                message=msg.transcript[k]
                completion = openai.ChatCompletion.create(
                 model    = "gpt-3.5-turbo",     # モデルを選択
                 messages = [
                            {"role": "system", "content": "句読点と読点を多く含めて応答するようにして下さい。また、1文あたりが長くならないようにして下さい。"},
                            {"role": "user", "content": message}
                            ],
                 max_tokens  = 1024,             # 生成する文章の最大単語数
                 n           = 1,                # いくつの返答を生成するか
                 stop        = None,             # 指定した単語が出現した場合、文章生成を打ち切る
                 temperature = 0.5,              # 出力する単語のランダム性（0から2の範囲） 0であれば毎回返答内容固定
                 stream=True
                )
                fullResponse = ""
                RealTimeResponce = ""   
                for chunk in completion:
                    text = chunk['choices'][0]['delta'].get('content')

                    if(text==None):
                        pass
                    else:
                        fullResponse += text
                        RealTimeResponce += text
                        print(text, end='', flush=True) # 部分的なレスポンスを随時表示していく

                        target_char = ["。","、","！", "？", "\n"]
                        for index, char in enumerate(RealTimeResponce):
                            if char in target_char:
                                pos = index + 2        # 区切り位置
                                sentence = RealTimeResponce[:pos]           # 1文の区切り
                                RealTimeResponce = RealTimeResponce[pos:]   # 残りの部分
                                # 1文完成ごとにテキストを読み上げる(遅延時間短縮のため)
                                self.pub.send_msg(sentence)
                                rospy.sleep( round(len(sentence)*0.3)  )
                                break
                            else:
                                pass

    # APIからの完全なレスポンスを返す



                
#                response = completion.choices[0].message.content
#                print(response)
#                self.pub.send_msg(response)
#                rospy.sleep( round(len(response)*0.3)  )
                self.start()

def main():
    # nodeの立ち上げ
    rospy.init_node('Node_name')

    # クラスの作成
    pub = myTTS()
    sub = mySTT(pub)
    sub.start()

    # ratesleep
    rate = rospy.Rate(40)
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == '__main__':
   main()
