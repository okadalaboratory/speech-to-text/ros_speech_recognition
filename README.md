# 音声認識パッケージ
Whisperによる音声認識ROSパッケージ

東京大学JSKの音声認識ROSパッケージを参考にしました<br>
[ros_speech_recognition](https://github.com/jsk-ros-pkg/jsk_3rdparty/tree/master/ros_speech_recognition)

## To Do
- 21 Feb. 2023<br>
~~認識の開始／終了をServiceで利用~~<br>
音声認識の中止
```
rosservice call /speech_recognition/stop "{}" 
```
音声認識の再開
```
rosservice call /speech_recognition/start "{}" 
```

## 音声の取得
音声は sound_play ノードから取得する

sound_playノードを別のPCで実行するには、下記のサイトを参考に環境の設定、launchファイルの修正を実施してください。<br>
[ROS Remote Launch](https://gitlab.com/okadalaboratory/how-to/remote-launch)


## インストール
```
# Install Whisper
pip3 install --upgrade pip
pip3 install git+https://github.com/openai/whisper.git 
pip3 install pyaudio  SpeechRecognition soundfile lxml beautifulsoup4 
pip3 install numpy==1.21
sudo apt-get install ros-noetic-catkin-virtualenv
sudo apt-get install ros-noetic-jsk-common
```

```
mkdir -p catkin_ws/src
cd ~/catkin_ws/src
git clone https://gitlab.com/okadalaboratory/speech-to-text/speech-recog.git
git clone https://github.com/jsk-ros-pkg/jsk_common_msgs.git
cd ~/catkin_ws
catkin_make
```
```
roslaunch ros_speech_recognition speech_recognition.launch
```

## 認識結果
**/speech_to_text**というトピックからSpeechRecognitionCandidatesというメッセージを受信する
```
import rospy
from std_msgs.msg import String
from speech_recognition_msgs.msg import SpeechRecognitionCandidates

def callback(msg):
    if len(msg.transcript) > 0: # 音声認識結果がある場合だけ処理。
        for k in range(0, len(msg.transcript)):
            rospy.loginfo(msg.transcript[k]) #認識した文字列
            rospy.loginfo(msg.confidence[k])

def result_sub():
    # 初期化し、ノードの名前を"result_subscriber"とする
    rospy.init_node('result_subscriber', anonymous=True)
    # "/speech_to_text"というトピックからSpeechRecognitionCandidatesというメッセージを受信する
    rospy.Subscriber('/speech_to_text', SpeechRecognitionCandidates, callback)

    rospy.spin()

if __name__ == '__main__':
    result_sub()
```
    
