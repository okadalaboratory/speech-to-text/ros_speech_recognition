#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

# 音声認識の結果を得る
# 結果入力トピック /speech_to_text

import rospy
from std_msgs.msg import String
from speech_recognition_msgs.msg import SpeechRecognitionCandidates


def callback(msg):
    if len(msg.transcript) > 0: # 音声認識結果がある場合だけ処理。
        for k in range(0, len(msg.transcript)):
            rospy.loginfo(msg.transcript[k]) #認識した文字列
            rospy.loginfo(msg.confidence[k])

def result_sub():
    # 初期化し、ノードの名前を"result_subscriber"とする
    rospy.init_node('result_subscriber', anonymous=True)
    # "/speech_to_text"というトピックからSpeechRecognitionCandidatesというメッセージを受信する
    rospy.Subscriber('/speech_to_text', SpeechRecognitionCandidates, callback)

    rospy.spin()

if __name__ == '__main__':
    result_sub()
